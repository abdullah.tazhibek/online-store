import {Categories} from "./components/Categories";
import {Header} from "./components/Header";
import {Products} from "./components/Products";
import {Footer} from "./components/Footer";

export function App() {
  return (
      <div className={"app-wrapper"}>
          <Header/>
          <Categories items={['Мясные', 'Вегетерианская', 'Гриль', 'Острые', 'Закрытые']}/>
          <h2 className={'pizzas-title'}>Все пиццы</h2>
          <Products/>
          <Footer/>
      </div>

  );
}