import pizza1 from '../assets/img/pizzas-img/image 2.png'

export function Products({pizzaName, pizzaPrice}) {
    return (
        <div className={'pizza'}>
            <img className={"pizza__img"} src={pizza1} alt="Чизбургер пицца"/>
            <h3 className={"pizza__name"}>Чизбургер пицца</h3>
            <div className={'pizza__prop'}>
                <div className={'pizza__height'}>
                    <div className={'active'}>тонкое</div>
                    <div>традиционное</div>
                </div>
                <div className={'pizza__radius'}>
                    <div className={'active'}>26 см</div>
                    <div>30 см</div>
                    <div>40 см</div>
                </div>
            </div>
            <div className={'price-cartBtn-block'}>
                <h3>от 1925 ₸</h3>
                <div>
                    <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12" fill="none">
                        <path d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373
                        4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2
                        11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
                              fill="#EB5A1E"/>
                    </svg>
                    Добавить
                </div>
            </div>
        </div>
    )
}
