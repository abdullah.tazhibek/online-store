import cart from '../assets/img/shopping-cart-icon.svg'
import line from '../assets/img/white-line.png'
import {useState} from "react";

export function Button() {
    const [price, setPrice] = useState(0)
    const [cartItems, setCartItems] = useState(0)

    return (
        <div className={'cart'}>
            <p className={'cart__price'}>2000 ₸</p>
            <img className={'cart__line'} src={line} alt={'line'}/>
            <img className={'cart__icon'} src={cart} alt="shopping-cart"/>
            <p className={'cart__items'}>{cartItems}</p>
        </div>
    )
}