import companyLogo from '../assets/img/company-logo.png'
import {Button} from "./Button";

export function Header() {
    return(
        <div className={'company__logo'}>
            <div className={'company__img'}>
                <img src={companyLogo} alt="company logo"/>
            </div>
            <div className={'company__txt'}>
                <p className={'company__title'}>REACT PIZZA</p>
                <p className={'company__subtitle'}>самая вкусная пицца во вселенной</p>
            </div>
            <div className={'company__button'}>
                <Button />
            </div>
            <hr/>
        </div>
    )
}