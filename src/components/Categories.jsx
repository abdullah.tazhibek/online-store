import {useState} from "react";
import {Sort} from "./Sort";

export function Categories({ items }) {
    const [activeItem, setActiveItem] = useState(null)

    const onSelectItem = (id) => {
        setActiveItem(id)
    }

    return (
        <div className={"categories"}>
            <ul className={"categories__list"}>
                <li className={activeItem === null ? 'active' : ''} onClick={() => onSelectItem(null)}>Все</li>
                {items && items.map((name, id) => (
                    <li className={activeItem === id ? 'active' : ''}
                        onClick={() => onSelectItem(id)}
                        key={`${name}_${id}`}>
                        {name}
                    </li>
                ))}
            </ul>
            <Sort/>
        </div>
    );
}
